package com.cdai.plugin.rapidg;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.*;

/**
 * Created by zhangfy on 2015/8/24.
 */
public class BaseAction extends AnAction {
	String fileName;

	@Override
	public void actionPerformed(AnActionEvent e) {
		// TODO: insert action logic here
		Application application = ApplicationManager.getApplication();
		final VirtualFile file = CommonDataKeys.VIRTUAL_FILE.getData(e.getDataContext());
		fileName = file.getName();
		try {
			InputStream inputStream = new FileInputStream(new File(file.getCanonicalPath()));
			String string = readXML(inputStream);
			showDialog(string.toString());
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	//读取public class PullService {  
	public  String readXML(InputStream inputStream) throws IOException {
		XmlPullParser parser = null;
		StringBuilder list = new StringBuilder();
		try {
			parser = XmlPullParserFactory.newInstance().newPullParser();

		} catch (XmlPullParserException e1) {
			e1.printStackTrace();
		}
		try {
			parser.setInput(inputStream, "UTF-8");
			int eventType = parser.getEventType();
			String currentString = null;
			while (eventType != XmlPullParser.END_DOCUMENT) {
				switch (eventType) {
					case XmlPullParser.START_DOCUMENT://文档开始事件,可以进行数据初始化处理  
						break;
					case XmlPullParser.START_TAG://开始元素事件  
						parseId(parser, list);
						break;
					case XmlPullParser.END_TAG://结束元素事件  

						break;
				}
				eventType = parser.next();
			}
			inputStream.close();
			return list.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list.toString();
	}

	private  void parseId(XmlPullParser parser, StringBuilder list){
			String node = parser.getName();
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < parser.getAttributeCount(); i++) {
				String attrName = parser.getAttributeName(i);//"android:id"
				String attrValue = parser.getAttributeValue(i);//"@+id/listView"
				if ("android:id".equals(attrName)) {//此处生成代码
					if (fileName.startsWith("activity") || fileName.startsWith("fragment")) {
						//						@TAInjectView(id = R.id.rg)
						stringBuilder.append("@TAInjectView(id = "+getId(attrValue) +")\n");
						stringBuilder.append(node + " " + attrValue.replace("@+id/", "").replace("@id/", "")+";");
						list.append(stringBuilder.toString() + "\n");
					}else{
						stringBuilder.append(node + " " + attrValue.replace("@+id/", "").replace("@id/", ""));
						stringBuilder.append("= ");
						String id = getId(attrValue);
						stringBuilder.append("holder.obtainView(convertView," + id + ");");
						System.out.println(stringBuilder.toString());
						list.append(stringBuilder.toString() + "\n");
					}
				}
			}
	}

	/**
	 * @param attrValue
	 * @return R.id.listView
	 */
	private String getId(String attrValue) {
		return attrValue.replace("@+id/", "R.id.").replace("@id/", "R.id.");
	}

	;

	public void showDialog(String msg) {
		Messages.showMessageDialog(msg, "generator", Messages.getInformationIcon());
	}
}
